import sys
import unicodedata


def print_unicode_table(words):
    print("decimal   hex   chr  {0:^40}".format("name"))
    print("-------  -----  ---  {0:-<40}".format(""))

    code = ord(" ")
    end = sys.maxunicode

    while code < end:
        c = chr(code)
        name = unicodedata.name(c, "*** unknown ***")
        sustain = True
        for word in words:
            if not (word is None) and not (word in name.lower()):
                sustain = False
        if sustain:
            print("{0:7}  {0:5X}  {0:^3c}  {1}".format(
                 code, name.title()))    
        code += 1


words = []
word = 1
len_arguments = len(sys.argv) - 1
if len_arguments > 0:
    if sys.argv[1] in ("-h", "--help"):
        print("usage: {0} [string]".format(sys.argv[0]))
        word = 0
    else:
        while len_arguments > 0:
            words.append(sys.argv[len_arguments].lower())
            len_arguments -= 1
else:
    words.append(None)
if word != 0:
    print_unicode_table(words)