#!/usr/bin/env python3
import random

i = 1000
my_str = ""
symbols = ('!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '-', '=', '+', '\\', '|', '{', '}', '[', ']', ';', ':', '\'', '"', ',', '.', '/', '?')
while i > 0:
	my_str +=  random.choice(symbols)
	i -= 1
print(my_str)
