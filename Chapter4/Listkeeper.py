#!/usr/bin/env python3

import os

def get_integer(message, name="integer", default=None, minimum=0,
                maximum=100, allow_zero=True):

    class RangeError(Exception): pass

    message += ": " if default is None else " [{0}]: ".format(default)
    while True:
        try:
            line = input(message)
            if not line and default is not None:
                return default
            i = int(line)
            if i == 0:
                if allow_zero:
                    return i
                else:
                    raise RangeError("{0} may not be 0".format(name))
            if not (minimum <= i <= maximum):
                raise RangeError("{0} must be between {1} and {2} "
                        "inclusive{3}".format(name, minimum, maximum,
                        (" (or 0)" if allow_zero else "")))
            return i
        except RangeError as err:
            print("ERROR", err)
        except ValueError as err:
            print("ERROR {0} must be an integer".format(name))

def get_string(message, name="string", default=None,
               minimum_length=0, maximum_length=80):
    message += ": " if default is None else " [{0}]: ".format(default)
    while True:
        try:
            line = input(message)
            if not line:
                if default is not None:
                    return default
                if minimum_length == 0:
                    return ""
                else:
                    raise ValueError("{0} may not be empty".format(
                                     name))
            if not (minimum_length <= len(line) <= maximum_length):
                raise ValueError("{0} must have at least {1} and "
                        "at most {2} characters".format(
                        name, minimum_length, maximum_length))
            return line
        except ValueError as err:
            print("ERROR", err) 

def load_file(filename):
	open(filename)

def printing_list(anylist):
	for nelement, element in enumerate(anylist.sort(str.lower), start=1):
		print('{0}. {1}\n'.format(nelement, element))

def main():
	files = [file_dir for file_dir in os.listdir('.') if file_dir.endswith('.lst')]
	if not files:
		new_file = get_string("Unfortunately in this directory have not any files with type `lst`, please enter name of new file", minimum_length = 1)
		if not new_file.endswith('.lst'):
			new_file += ".lst"
		files.append(new_file)
		print(new_file)
	
	for nfile, efile in enumerate(files, start=1):
		print('{0}. {1}\n'.format(nfile, efile))
	key = get_integer('Choice the file with whom you want to work.\n Enter a key from 1 and more or 0 for exit from the program\n')
	load_file(files[key - 1])

main()