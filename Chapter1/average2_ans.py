#!/usr/bin/env python3
def sum_arr(arr):
	sum = 0;
	for el in arr:
		try:
			sum += int(el)
		except ValueError as err:
			print(err)
	return sum

def sort(arr):	
	k = 0
	length = len(arr)
	while length > 0:
		while k < length - 1:
			if arr[k] > arr[k+1]:
			 	temp = arr[k+1]
			 	arr[k+1] = arr[k]
			 	arr[k] = temp
			k += 1
		length -= 1
		k = 0
	return arr

numbers = []

while True:
	try:
		entered = input("enter a number or Enter to finish: ")
		if len(entered) == 0:
			break
		numbers.append(int(entered))
	except ValueError as err:
		print(err)
	

if len(numbers) > 0:
	print("count = " + str(len(numbers)) + " sum = " + str(sum_arr(numbers)) + " lowest = " + str(min(numbers)) + " highest = " + str(max(numbers)) + " mean = " + str(sum_arr(numbers)/len(numbers)))
