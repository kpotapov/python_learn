#! /usr/bin/env python3

import random

articles = ["a", "the", "her", "his", "another"]
nouns = ["cat", "dog", "man", "woman"]
verbs = ["sang", "ran", "jumped"]
adverbs = ["loudly", "quetly", "well", "badly"]

i = 0
while i < 5:
	line = ""
	line += random.choice(articles) + " " + random.choice(nouns) + " " + random.choice(verbs)
	if random.randint(0,1):
		line += " " + random.choice(adverbs)
	print(line)
	i += 1