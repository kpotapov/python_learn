#! /usr/bin/env python3

import random
import sys

articles = ["a", "the", "her", "his", "another"]
nouns = ["cat", "dog", "man", "woman"]
verbs = ["sang", "ran", "jumped"]
adverbs = ["loudly", "quetly", "well", "badly"] 

num_line = 5

if len(sys.argv) > 1:
	try:
		num_line_user = int(sys.argv[1])
		if 0 < num_line_user <= 10:
			num_line = num_line_user

	except ValueError as err:
		print(err)
i = 0

while i < num_line:
	line = ""
	line += random.choice(articles) + " " + random.choice(nouns) + " " + random.choice(verbs)
	if random.randint(0,1):
		line += " " + random.choice(adverbs)
	print(line)
	i += 1

